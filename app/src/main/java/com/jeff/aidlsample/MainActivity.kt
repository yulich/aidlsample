package com.jeff.aidlsample

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.*
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import com.jeff.server.IMyAidlInterface
import com.jeff.server.MyAidlService
import com.jeff.server.MyContact
import com.log.JFLog
import java.lang.Exception

/**
 * https://developer.android.com/guide/components/bound-services?hl=zh-CN
 */
class MainActivity : AppCompatActivity() {

    var aidl: IMyAidlInterface? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bindService()
    }

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            aidl = IMyAidlInterface.Stub.asInterface(service)

            JFLog.d("onServiceConnected: $componentName")

            JFLog.d("Result: ${aidl!!.add(1, 2)}")

            val mutableMap = mutableMapOf("v1" to "One", "v2" to "Two")
            JFLog.d("Result: ${aidl!!.testMap(mutableMap)["v3"]}")

            /*
            val contact = MyContact(0, "Yu")
            JFLog.d("Result: ${aidl!!.testParcelable(contact)}")
            */
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            aidl = null

            JFLog.d("onServiceDisconnected: $componentName")
        }
    }

    private fun bindService() {
        val intent = Intent()

        // 三種不同設定方式
        intent.setClass(this, MyAidlService::class.java)
//        intent.setClassName("com.jeff.aidlsample", "com.jeff.server.MyAidlService")
//        intent.component = ComponentName("com.jeff.aidlsample", "com.jeff.server.MyAidlService")
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
    }

    /* ------------------------------------------------------------------------------------------ */

    // 定義傳入伺服端的'Messenger'
    private val clientMessenger = Messenger(object : Handler() {
        override fun handleMessage(msg: Message) {

            super.handleMessage(msg)
        }
    })

    private var serviceConnection2 = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, binder: IBinder?) {
            JFLog.d("onServiceConnected: $name, $binder")

            val service = Messenger(binder)
            val msg = Message.obtain(null, 0x100, null)

            // 設定傳入伺服端的'Messenger'
            msg.replyTo = clientMessenger

            try {
                service.send(msg)
            } catch (e: RemoteException) {
                JFLog.e(e)
            }
        }

        override fun onServiceDisconnected(p0: ComponentName?) {
            // 當與服務的連接意外中斷（例如服務崩潰或被終止）時，Android 系統會調用該方法。當客戶端取消綁定時，系統不會調用該方法。
            JFLog.d("onServiceDisconnected: $p0")
        }
    }

    fun bindMessengerService(btn: View) {
        val it = Intent()
        it.setClassName("io.busniess.va.sandhook", "io.virtualapp.delegate.MessengerService")
        bindService(it, serviceConnection2, Context.BIND_AUTO_CREATE)
    }

    fun unbindMessengerService(btn: View) {
        try {
            unbindService(serviceConnection2)
        } catch (e: Exception) {
            JFLog.e(e)
        }
    }
}
