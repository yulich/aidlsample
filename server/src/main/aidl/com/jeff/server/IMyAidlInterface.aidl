// IMyAidlInterface.aidl
package com.jeff.server;

// Declare any non-default types here with import statements
parcelable MyContact;

interface IMyAidlInterface {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    void basicTypes(int anInt, long aLong, boolean aBoolean, float aFloat,
            double aDouble, String aString);

    int add(int v1, int v2);

    Map testMap(in Map datas);

    // MyContact testParcelable(in MyContact contact);
}
