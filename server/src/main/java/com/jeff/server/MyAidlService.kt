package com.jeff.server

import android.app.Service
import android.content.Intent
import android.os.IBinder

/**
 * https://developer.android.com/guide/components/aidl?hl=zh-cn
 */
class MyAidlService : Service() {

    override fun onBind(p0: Intent?): IBinder? {
        return iBinder
    }

    private val iBinder = object : IMyAidlInterface.Stub() {
        override fun basicTypes(
            anInt: Int,
            aLong: Long,
            aBoolean: Boolean,
            aFloat: Float,
            aDouble: Double,
            aString: String?) {
        }

        override fun add(v1: Int, v2: Int): Int {
            return v1 + v2
        }

        override fun testMap(datas: MutableMap<Any?, Any?>?): MutableMap<Any?, Any?> {
            val v1 = datas!!["v1"] as String
            val v2 = datas!!["v2"] as String
            datas["v3"] = "${v1}_$v2"
            return datas
        }

        /*
        override fun testParcelable(contact: MyContact?): MyContact {
            return MyContact(1, "Jeff")
        }
        */
    }
}