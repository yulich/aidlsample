package com.jeff.server;

import android.os.Parcel;
import android.os.Parcelable;

public class MyContact implements Parcelable {

    private int id;
    private String name;

    public MyContact(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public MyContact() {
    }

    protected MyContact(Parcel in) {
        // 注意順序
        this.id = in.readInt();
        this.name = in.readString();
    }

    public static final Creator<MyContact> CREATOR = new Creator<MyContact>() {
        @Override
        public MyContact createFromParcel(Parcel source) {
            return new MyContact(source);
        }

        @Override
        public MyContact[] newArray(int size) {
            return new MyContact[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flag) {
        // 注意順序
        dest.writeInt(id);
        dest.writeString(name);
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", name='" + name + '\'' + '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
